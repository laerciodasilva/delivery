<?php

use Illuminate\Database\Seeder;
use Delivery\Models\Product;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Product::class, 20)->create();
    }
}
