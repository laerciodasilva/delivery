<?php

use Illuminate\Database\Seeder;
use Delivery\Models\User;
use Delivery\Models\Client;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Administrador',
            'email' => 'laerciokelson@gmail.com',
            'password' => bcrypt('123'),
            'remember_token' => bcrypt(rand(1,10)),
            'role' => 'admin'
        ])->client()->save(factory(Client::class)->make());
        
        factory(User::class)->create([
            'name' => 'Entregador',
            'email' => 'laercio_kelson@hotmail.com',
            'password' => bcrypt('123'),
            'remember_token' => bcrypt(rand(1, 10))
        ])->client()->save(factory(Client::class)->make());
        
        factory(User::class,10)->create()->each(function($u){
            $u->client()->save(factory(\Delivery\Models\Client::class)->make());
        });
    }
}
