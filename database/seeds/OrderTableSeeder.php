<?php

use Illuminate\Database\Seeder;
use Delivery\Models\Order;
use Delivery\Models\OrderItem;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(Category::class, 10)->create();
        factory(Order::class, 10)->create()->each(function($o){
            for($i=0; $i <= 2; $i++) {
                $o->items()->save(factory(OrderItem::class)->make([
                    'product_id' => rand(1, 5),
                    'quantidade' => 2,
                    'price' => 20,
                ]));
            }
        });
    }
}
