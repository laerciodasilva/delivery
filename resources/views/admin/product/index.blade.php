@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Produtos 
                                <a href="{{route('admin.product.new')}}" class="pull-right">Novo Produto</a>
                            </div>

                            <div class="panel-body">
                                <table class="table table-striped table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Produto</th>
                                            <th>Categoria</th>
                                            <th>Preço</th>
                                            <th colspan="2" class="text-center">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $product)
                                        <tr>
                                            <td>{{$product->id}}</td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->categories->name}}</td>
                                            <td>{{$product->price}}</td>
                                            <td><a href="{{route('admin.product.edit', ['id' => $product->id])}}" class="btn">Editar</a></td>
                                            <td><a href="{{route('admin.product.destroy', ['id' => $product->id])}}" class="btn">Excluir</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {!!$products->render()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection