@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
                       @include('errors.checkError')
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Editar Produto - {{$product->name}}
                                <a href="{{url()->previous()}}" class="pull-right">Voltar</a>
                            </div>
                            <div class="panel-body">
                                <!--Model faz um bind nos campos dos formulário-->
                                {!! Form::model($product, ['route' => ['admin.product.update', $product->id], 'method' => 'put']) !!}
                                @include('admin.product._form')
                                
                                <div class="form-group">
                                    {!! Form::submit('Gravar', ['class' => 'btn btn-primary']) !!}
                                </div>
                                
                                {!! Form::close()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection