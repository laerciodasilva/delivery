@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
                       @include('errors.checkError')
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Editar Pedido
                                <a href="{{url()->previous()}}" class="pull-right">Voltar</a>
                            </div>
                            <div class="panel-body">
                                <h1>Pedido #{{$order->id}}</h1>
                                <h2>Cliente #{{$order->client->user->name}}</h2>
                                <h2>Data #{{$order->created_at}}</h2>
                                <h3>Entregar em:</h3>
                                <address>
                                    <strong>Endereço</strong><br/>
                                    {{$order->client->address}}<br/>
                                    <strong>Cidade</strong><br/>
                                    {{$order->client->city}}<br/>
                                    <strong>Estado</strong><br/>
                                    {{$order->client->state}}<br/>
                                    <abbr title="Telefone">Tel: </abbr>{{$order->client->phone}}
                                </address>
                                <!--Model faz um bind nos campos dos formulário-->
                                {!! Form::model($order, ['route' => ['admin.order.update', $order->id], 'method' => 'put']) !!}
                                <div class="form-group">
                                    {!! Form::label('status', 'Status') !!}
                                    {!! Form::select('status', $listaStatus, null, ['class' => 'form-control'])!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Entregador', 'Entregador') !!}
                                    {!! Form::select('user_deliveryman_id', $deliveryman, null, ['class' => 'form-control'])!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Gravar', ['class' => 'btn btn-primary']) !!}
                                </div>
                                
                                {!! Form::close()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection