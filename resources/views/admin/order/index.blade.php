@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Pedidos 
                            </div>

                            <div class="panel-body">
                                <table class="table table-striped table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Cliente</th>
                                            <th>R$ Total</th>
                                            <th>Data</th>
                                            <th>Itens</th>
                                            <th>Status</th>
                                            <th>Entregador</th>
                                            <th class="text-center">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orders as $order)
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->client->user->name}}</td>
                                            <td>R$ {{$order->total}}</td>
                                            <td>{{$order->created_at}}</td>
                                            <td>
                                                <ol>
                                                @foreach($order->items as $item)
                                                    <li>{{$item->product->name}}</li>
                                                @endforeach
                                                </ol>
                                            </td>
                                            <td>{{$order->status}}</td>
                                            <td>
                                                @if ($order->deliveryman)
                                                    {{$order->deliveryman->name}}
                                                @else (count($records) > 1)
                                                    ---
                                                @endif    
                                            </td>
                                            <td><a href="{{route('admin.order.edit', ['id' => $order->id])}}" class="btn">Editar</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {!!$orders->render()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection