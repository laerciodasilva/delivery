@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
                       @include('errors.checkError')
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Editar Categoria - {{$category->name}}
                                <a href="{{url()->previous()}}" class="pull-right">Voltar</a>
                            </div>
                            <div class="panel-body">
                                <!--Model faz um bind nos campos dos formulário-->
                                {!! Form::model($category, ['route' => ['admin.category.update', $category->id], 'method' => 'put']) !!}
                                <div class="form-group">
                                    {!! Form::label('Name', 'Nome') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control'])!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Gravar', ['class' => 'btn btn-primary']) !!}
                                </div>
                                {!! Form::close()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection