@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Categorias 
                                <a href="{{route('admin.category.new')}}" class="pull-right">Nova Categoria</a>
                            </div>

                            <div class="panel-body">
                                <table class="table table-striped table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th colspan="2" class="text-center">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $category)
                                        <tr>
                                            <td>{{$category->id}}</td>
                                            <td>{{$category->name}}</td>
                                            <td><a href="{{route('admin.category.edit', ['id' => $category->id])}}" class="btn">Editar</a></td>
                                            <td><a href="{{route('admin.category.destroy', ['id' => $category->id])}}" class="btn">Excluir</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {!!$categories->render()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection