@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Produtos 
                                <a href="{{route('admin.client.new')}}" class="pull-right">Novo Cliente</a>
                            </div>

                            <div class="panel-body">
                                <table class="table table-striped table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Cliente</th>
                                            <th>Telefone</th>
                                            <th>Estado</th>
                                            <th>Cidade</th>
                                            <th>CEP</th>
                                            <th colspan="2" class="text-center">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($clients as $client)
                                        <tr>
                                            <td>{{$client->id}}</td>
                                            <td>{{$client->user->name}}</td>
                                            <td>{{$client->phone}}</td>
                                            <td>{{$client->state}}</td>
                                            <td>{{$client->city}}</td>
                                            <td>{{$client->zipcode}}</td>
                                            <td><a href="{{route('admin.client.edit', ['id' => $client->id])}}" class="btn">Editar</a></td>
                                            <td><a href="{{route('admin.client.destroy', ['id' => $client->id])}}" class="btn">Excluir</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {!!$clients->render()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection