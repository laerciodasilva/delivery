@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Cupoms 
                                <a href="{{route('admin.cupom.new')}}" class="pull-right">Novo Cupom</a>
                            </div>

                            <div class="panel-body">
                                <table class="table table-striped table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Valor</th>
                                            <th colspan="2" class="text-center">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cupoms as $cupom)
                                        <tr>
                                            <td>{{$cupom->id}}</td>
                                            <td>{{$cupom->code}}</td>
                                            <td>R$ {{$cupom->value}}</td>
                                            <td><a href="{{route('admin.cupom.edit', ['id' => $cupom->id])}}" class="btn">Editar</a></td>
                                            <td><a href="{{route('admin.cupom.destroy', ['id' => $cupom->id])}}" class="btn">Excluir</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {!!$cupoms->render()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection