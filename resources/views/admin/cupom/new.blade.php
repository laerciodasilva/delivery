@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
                        @include('errors.checkError')
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Novo Cupom 
                                <a href="{{url()->previous()}}" class="pull-right">Voltar</a>
                            </div>
                            <div class="panel-body">
                                {!! Form::open(['route' => 'admin.cupom.store']) !!}
                                @include('admin.cupom._form')
                                <div class="form-group">
                                    {!! Form::submit('Gravar', ['class' => 'btn btn-primary']) !!}
                                </div>
                                {!! Form::close()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection