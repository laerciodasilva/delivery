@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
                       @include('errors.checkError')
			<div class="panel panel-default">
                            <div class="panel-heading">
                                Editar Cupom - {{$cupom->code}}
                                <a href="{{url()->previous()}}" class="pull-right">Voltar</a>
                            </div>
                            <div class="panel-body">
                                <!--Model faz um bind nos campos dos formulário-->
                                {!! Form::model($cupom, ['route' => ['admin.cupom.update', $cupom->id], 'method' => 'put']) !!}
                                @include('admin.cupom._form')
                                <div class="form-group">
                                    {!! Form::submit('Gravar', ['class' => 'btn btn-primary']) !!}
                                </div>
                                {!! Form::close()!!}
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection