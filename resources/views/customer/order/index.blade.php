@extends('app')

@section('content')
<div class="container">
    <h3>Meu(s) Pedido(s)</h3>
    <a href="{{route('customer.order.new')}}" class="pull-right">Novo pedido</a>
    <table class="table table-striped table-condensed table-hover">
        <thead>
            <tr>
                <th>#</th>
                <!--<th>Produto</th>-->
                <th>Total</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->total}}</td>
                    <td>{{$order->status}}</td>
                </tr>
                @endforeach
            </tbody>
        </tbody>
    </table>
    {!!$orders->render()!!}
</div>
@endsection