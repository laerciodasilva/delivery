<?php

namespace Delivery\Transformers;

use League\Fractal\TransformerAbstract;
use Delivery\Models\Cupom;

/**
 * Class CupomTransformer
 * @package namespace Delivery\Transformers;
 */
class CupomTransformer extends TransformerAbstract
{
    //protected $defaultIncludes = ['order'];
    /**
     * Transform the \Cupom entity
     * @param \Cupom $model
     *
     * @return array
     */
    public function transform(Cupom $model)
    {
        return [
            'id'         => (int) $model->id,
            'code'       => $model->id,
            'value'      => (float)$model->id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
