<?php

namespace Delivery\Transformers;

use League\Fractal\TransformerAbstract;
use Delivery\Models\Order;

/**
 * Class OrderTransformer
 * @package namespace Delivery\Transformers;
 */
class OrderTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['client', 'cupom', 'items'];

    /**
     * Transform the \Order entity
     * @param \Order $model
     *
     * @return array
     */
    public function transform(Order $model)
    {
        return [
            'id'         => (int) $model->id,
            'total'      => (float) $model->total,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            
        ];
    }
    
    /**
     * Serialização de relacionamento
     * @param Order $order
     * @return type
     */
    public function includeCupom(Order $order)
    {
        if (!$order->cupom) {
            return null;
        }
        //Quando é um para um
        return $this->item($order->cupom, new CupomTransformer());
    }
    
    /**
     * Serialização de relacionamento
     * @param Order $order
     * @return type
     */
    public function includeClient(Order $order)
    {
        if (!$order->client) {
            return null;
        }
        //Quando é um para um
        return $this->item($order->client, new ClientTransformer());
    }
    
    /**
     * Serialização de relacionamento
     * @param Order $order
     * @return type
     */
    public function includeItems(Order $order)
    {
        return $this->collection($order->items, new OrderItemTransformer());
    }
}
