<?php

namespace Delivery\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Primeiro paramétro é classe abstrata(que não pode ser instânciada)
         * Segundo paramétro é classe concreta(que pode ser instânciada)
         * Obs:Quando pretende que uma classe abstrata seja intânciada na dependência de injeção ou..., é preciso criar provider e, 
         * informe para laravel a classe que será instânciada
         */
        $this->app->bind(
        \Delivery\Repositories\ClientRepository::class, 
        \Delivery\Repositories\ClientRepositoryEloquent::class
        );
        
        $this->app->bind(
        \Delivery\Repositories\ProductRepository::class, 
         \Delivery\Repositories\ProductRepositoryEloquent::class
        );
        $this->app->bind(
        \Delivery\Repositories\CategoryRepository::class, 
         \Delivery\Repositories\CategoryRepositoryEloquent::class
        );
        
         $this->app->bind(
         \Delivery\Repositories\OrderRepository::class, 
         \Delivery\Repositories\OrderRepositoryEloquent::class
        );
         
         $this->app->bind(
         \Delivery\Repositories\OrderItemRepository::class, 
         \Delivery\Repositories\OrderItemRepositoryEloquent::class
        );
             
        $this->app->bind(
           \Delivery\Repositories\UserRepository::class, 
           \Delivery\Repositories\UserRepositoryEloquent::class
        );
        
        $this->app->bind(
        \Delivery\Repositories\CupomRepository::class, 
        \Delivery\Repositories\CupomRepositoryEloquent::class
        );
    }
}
