<?php
/**
 * Onde ficará toda à valição do cliente
 */
namespace Delivery\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Description of ClientValidator
 *
 * @author laercio
 */
class CategoryValidator extends LaravelValidator{
    
    protected $rules = [
        'nome' => 'required|max:255'
    ];
}
