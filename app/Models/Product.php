<?php

namespace Delivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'name',
        'description',
        'price',
        'category_id',
    ];
    
    
    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

}
