<?php

namespace Delivery\Models;

use Illuminate\Database\Eloquent\Model;

class Core extends Model
{
    public static function status()
    {
        return [
            '' => 'Selecione',
            0 => 'Pendente',
            1 => 'Preparando',
            2 => 'A caminho',
            3 => 'Entregue',
            4 => 'Cancelado',
        ];
    }
    
    public static function getStatus($parametro)
    {         if ($parametro) {
            $status = [
                0 => 'Pendente',
                1 => 'Preparando',
                2 => 'A caminho',
                3 => 'Entregue',
                4 => 'Cancelado',
            ];
            $keys = (array_keys($status));
            $search = array_search($parametro, $keys);
            if ($search != false) {
               return $status[$search]; 
            }
        }
        return $parametro;
    }
}
