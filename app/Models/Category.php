<?php

namespace Delivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;

    //Permite inserção dos dados no método create
    protected $fillable = [
      'name'  
    ];
    
    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
