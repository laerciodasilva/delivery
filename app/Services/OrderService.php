<?php

namespace Delivery\Services;

use Delivery\Repositories\OrderRepository;
use Delivery\Repositories\ProductRepository;
use Delivery\Repositories\CupomRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Delivery\Models\Order;

class OrderService 
{
    
    protected $repository;
    protected $productRepository;
    protected $cupomRepository;

    public function __construct(
        OrderRepository $repository, 
        ProductRepository $productRepository,
        CupomRepository $cupomRepository
    )
    {
        $this->repository        = $repository;
        $this->productRepository = $productRepository;
        $this->cupomRepository   = $cupomRepository;
    }
    
    public function update(array $data, $id)
    {
        $this->repository->update($data, $id);
        
        $userId = $this->repository->find($id, ['user_id'])->user_id;
        
        $this->userRepository->update($data['user'], $userId);
    }
    
    public function create(array $data)
    {
        DB::beginTransaction();
        try {
            dd($data);
            if (isset($data['cupom_code'])) {
                $cupom            = $this->cu->findByField('code', $data['cupom_code'])->first();
                $data['cupom_id'] = $cupom->id;
                $cupom->used      = 1;
                $cupom->save();
                unset($data['cupom_code']);
            }
            if ($data['items']) {
                $items = $data['items'];
                unset($data['items']);
            }
            $order = $this->repository->create($data);
            $total = 0;

            foreach ($items as $item) {
                $item['price'] = $this->productRepository->find($item['product_id'])->price;

                $order->items()->create($item);
                $total += $item['price'] * $item['quantidade'];
            }
            $order->total = $total;
            if (isset($cupom)) {
                $order->total = $total - $cupom->value;
            }
            $order = $order->save();   
            DB::commit();
            return $order;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex->getMessage();
        }
    }
    
    public function showDeliveryman($id, $deliveryman)
    {
        $result = $this->repository->with(['items', 'items.product'])->findWhere(['id' => $id, 'user_deliveryman_id' => $deliveryman]);
        
        if ($result instanceof Collection) {
            $result = $result->first();
        }
        
        return $result;
    }
    
    public function updateStatus($id, $deliveryman, $status)
    {
        $order = $this->repository->with(['items', 'items.product'])->findWhere(['id' => $id, 'user_deliveryman_id' => $deliveryman]);
        
        if ($order instanceof Collection) {
            $order = $order->first();
            if ($order) {
                $order = Order::find($order->id);
                $order->status = $status;
                $order->save();
                return $order;
            }
        }
        return false;
        
    }
}
