<?php

namespace Delivery\Services;

use Delivery\Repositories\ClientRepository;
use Delivery\Repositories\UserRepository;

class ClientService 
{
    
    protected $repository;
    protected $userRepository;
    
    public function __construct(ClientRepository $repository, UserRepository $userRepository) 
    {
        $this->repository     = $repository;
        $this->userRepository = $userRepository;
    }
    
    public function update(array $data, $id)
    {
        $this->repository->update($data, $id);
        
        $userId = $this->repository->find($id, ['user_id'])->user_id;
        
        $this->userRepository->update($data['user'], $userId);
    }
    
    public function create(array $data)
    {
        $senha = 123;
        if (strpos($data['user']['name'], ' ') !== false) {
            list($senha) = explode(' ', $data['user']['name']);
        }else {
            $senha = $data['user']['name'];
        }
        $data['user']['password'] = bcrypt($senha);
        $user = $this->userRepository->create($data['user']);
        
        $data['user_id'] = $user->id;
        $this->repository->create($data);
    }
}
