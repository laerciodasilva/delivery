<?php

namespace Delivery\Http\Controllers;

use Illuminate\Http\Request;
use Delivery\Repositories\OrderRepository;
use Delivery\Repositories\UserRepository;
use Delivery\Models\Core;

class OrderController extends Controller
{
    protected $repository;
    protected $userRepository;
    
    
    public function __construct(OrderRepository $respository, UserRepository $userRepository) 
    {
        $this->repository     = $respository;
        $this->userRepository = $userRepository;
    }
    
    public function index()
    {
        $orders = $this->repository->orderBy('id', 'desc')->paginate(10);
        return view('admin.order.index', compact('orders'));
    }
    
    public function edit($id)
    {
        $order = $this->repository->find($id);
        
        $listaStatus = Core::status();
        
        $deliveryman = $this->userRepository->orderBy('name')->findWhere(['role' => 'deliveryman'])->lists('name', 'id');
       
        return view('admin.order.edit', compact('order', 'listaStatus', 'deliveryman'));
    }
    
    public function update(Request $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return redirect()->route('admin.order.index');
    }
}
