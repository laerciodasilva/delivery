<?php

namespace Delivery\Http\Controllers;

use Delivery\Repositories\ClientRepository;
Use Delivery\Repositories\UserRepository;
use Delivery\Services\ClientService;
use Delivery\Http\Requests\AdminClientRequest;

class ClientController extends Controller
{
    protected $repository;
    protected $userRepository;
    protected $service;


    public function __construct(ClientRepository $repository, UserRepository $userRepository, ClientService $service) 
    {
        $this->repository     = $repository;
        $this->userRepository = $userRepository;
        $this->service        = $service;
    }
    public function index()
    {
        $clients = $this->repository->orderBy('id', 'desc')->with(['user'])->paginate(10);
        return view('admin.client.index', compact('clients'));
    }
    
    public function create()
    {
        return view('admin.client.new');
    }
    
    public function store(AdminClientRequest $request)
    {
        $this->service->create($request->all());
        
        return redirect()->route('admin.client.index');
    }
    
    public function edit($id)
    {
        $client = $this->repository->find($id);
        return view('admin.client.edit', compact('client', 'users'));
    }
    
    public function update(AdminClientRequest $request, $id)
    {
        $this->service->update($request->all(), $id);
        return redirect()->route('admin.client.index');
    }
    
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.client.index');
    }
}
