<?php

namespace Delivery\Http\Controllers;

use Delivery\Repositories\CupomRepository;
use Delivery\Http\Requests\AdminCupomRequest;

class CupomController extends Controller
{
    protected $repository;
    
    public function __construct(CupomRepository $repository) {
        $this->repository = $repository;
    }
    public function index()
    {
        $cupoms = $this->repository->orderBy('id', 'desc')->paginate(10);
        return view('admin.cupom.index', compact('cupoms'));
    }
    
    public function create()
    {
        return view('admin.cupom.new');
    }
    
    public function store(AdminCupomRequest $request)
    {
        $this->repository->create($request->all());
        
        return redirect()->route('admin.cupom.index');
    }
    
    public function edit($id)
    {
        $cupom = $this->repository->find($id);
        return view('admin.cupom.edit', compact('cupom'));
    }
    
    public function update(AdminCupomRequest $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return redirect()->route('admin.cupom.index');
    }
    
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.cupom.index');
    }
}
