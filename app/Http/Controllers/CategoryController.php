<?php

namespace Delivery\Http\Controllers;

use Delivery\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Delivery\Http\Requests\AdminCategoryRequest;

class CategoryController extends Controller
{
    protected $repository;
    
    public function __construct(CategoryRepository $repository) {
        $this->repository = $repository;
    }
    public function index()
    {
        $categories = $this->repository->orderBy('id', 'desc')->paginate(10);
        return view('admin.category.index', compact('categories'));
    }
    
    public function create()
    {
        return view('admin.category.new');
    }
    
    public function store(AdminCategoryRequest $request)
    {
        $this->repository->create($request->all());
        
        return redirect()->route('admin.category.index');
    }
    
    public function edit($id)
    {
        $category = $this->repository->find($id);
        return view('admin.category.edit', compact('category'));
    }
    
    public function update(AdminCategoryRequest $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return redirect()->route('admin.category.index');
    }
    
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.category.index');
    }
}
