<?php

namespace Delivery\Http\Controllers;

use Delivery\Repositories\OrderRepository;
use Delivery\Repositories\UserRepository;
use Delivery\Repositories\ProductRepository;
use Delivery\Services\OrderService;
use Illuminate\Support\Facades\Auth;
use Delivery\Http\Requests\Api\CheckoutRequest;

class CheckoutController extends Controller
{
    protected $orderRepository;
    protected $userRepository;
    protected $productRepository;
    protected $orderService;
    
    public function __construct(
        OrderRepository $orderRepository,
        UserRepository  $userRepository,
        ProductRepository  $productRepository,
        OrderService  $orderService
    ) 
    {
        $this->orderRepository   = $orderRepository;
        $this->userRepository    = $userRepository;
        $this->productRepository = $productRepository;
        $this->orderService      = $orderService;
    }
    
    public function index() 
    {
        $clientId = $this->userRepository->find(Auth::user()->id)->client->id;
        $orders   = $this->orderRepository->scopeQuery(function($query) use($clientId){
            return $query->where('client_id', $clientId);
        })->with(['items', 'items.product'])->paginate();
        return view('customer.order.index', compact('orders'));
    }

    public function create()
    {
        $products = $this->productRepository->orderBy('id')->all(['name', 'price', 'id']);
        return view('customer.order.new', compact('products'));
    }
    
    public function store(CheckoutRequest $request)
    {
        $data              = $request->all();
        $clientId          = $this->userRepository->find(Auth::user()->id)->client->id;
        $data['client_id'] = $clientId;
        $this->orderService->create($data);
        
        return redirect()->route('customer.order.index');
    }
    
   
}
