<?php

namespace Delivery\Http\Controllers\Api\Deliveryman;

use Delivery\Http\Controllers\Controller;
use Delivery\Repositories\OrderRepository;
use Delivery\Services\OrderService;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Illuminate\Http\Request;

class DeliverymanCheckoutController extends Controller
{
    protected $orderRepository;
    protected $orderService;
    protected $with =['items'];
    
    public function __construct(
        OrderRepository $orderRepository,
        OrderService  $orderService
    ) 
    {
        $this->orderRepository   = $orderRepository;
        $this->orderService      = $orderService;
    }
    
    public function index() 
    {
        $deliveryman = Authorizer::getResourceOwnerId();
        $orders   = $this->orderRepository->skipPresenter(false)->with($this->with)->scopeQuery(function($query) use($deliveryman){
            return $query->where('user_deliveryman_id', $deliveryman);
        })->paginate();
        return $orders;
    }
    
   
    public function show($id)
    {
        $deliveryman = Authorizer::getResourceOwnerId();
        return $this->orderService->skipPresenter(false)->showDeliveryman($id, $deliveryman);
    }
    
    public function updateStatus(Request $request, $id)
    {
        $deliveryman = Authorizer::getResourceOwnerId();
        $order       = $this->orderService->skipPresenter(false)->updateStatus($id, $deliveryman, $request->get('status'));
        
        if($order) {
            return $order;
        }
        
        abort('404', 'Order não encontrado');
    }
   
}
