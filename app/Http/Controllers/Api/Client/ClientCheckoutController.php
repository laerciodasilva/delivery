<?php

namespace Delivery\Http\Controllers\Api\Client;

use Delivery\Http\Controllers\Controller;
use Delivery\Repositories\OrderRepository;
use Delivery\Repositories\UserRepository;
use Delivery\Services\OrderService;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Delivery\Http\Requests\Api\CheckoutRequest;

class ClientCheckoutController extends Controller
{
    protected $orderRepository;
    protected $userRepository;
    protected $orderService;
    protected $with =['client', 'cupom', 'items'];
    
    public function __construct(
        OrderRepository $orderRepository,
        UserRepository  $userRepository,
        OrderService  $orderService
    ) 
    {
        $this->orderRepository   = $orderRepository;
        $this->userRepository    = $userRepository;
        $this->orderService      = $orderService;
    }
    
    public function index() 
    {
        $clientId = $this->userRepository->find(Authorizer::getResourceOwnerId())->client->id;
        $orders   = $this->orderRepository->skipPresenter(false)->with($this->with)->scopeQuery(function($query) use($clientId){
            return $query->where('client_id', $clientId);
        })->paginate();
        return $orders;
    }
    
    public function store(CheckoutRequest $request)
    {
        $data              = $request->all();
        $clientId          = $this->userRepository->find(Authorizer::getResourceOwnerId())->client->id;
        $data['client_id'] = $clientId;
        $order             = $this->orderService->create($data);
        return $this->orderRepository->skipPresenter()->find($order->id);
    }
    
    public function show($id)
    {
        return $this->orderRepository->skipPresenter(false)->with($this->with)->find($id);
    }
   
}
