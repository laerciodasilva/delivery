<?php

namespace Delivery\Http\Controllers;

use Delivery\Repositories\ProductRepository;
use Delivery\Repositories\CategoryRepository;
use Delivery\Http\Requests\AdminProductRequest;

class ProductController extends Controller
{
    protected $repository;
    protected $categoryRepository;


    public function __construct(ProductRepository $repository, CategoryRepository $categoryRepository) {
        $this->repository         = $repository;
        $this->categoryRepository = $categoryRepository;
    }
    public function index()
    {
        $products = $this->repository->orderBy('id', 'desc')->with(['categories'])->paginate(10);
       
        return view('admin.product.index', compact('products'));
    }
    
    public function create()
    {
        $categories = $this->categoryRepository->lists('name', 'id');
        return view('admin.product.new', compact('categories'));
    }
    
    public function store(AdminProductRequest $request)
    {
        $this->repository->create($request->all());
        
        return redirect()->route('admin.product.index');
    }
    
    public function edit($id)
    {
        $product    = $this->repository->find($id);
        $categories = $this->categoryRepository->lists('name', 'id');
        return view('admin.product.edit', compact('product', 'categories'));
    }
    
    public function update(AdminProductRequest $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return redirect()->route('admin.product.index');
    }
    
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.product.index');
    }
}
