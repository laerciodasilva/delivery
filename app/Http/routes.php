<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin', 'middleware' => 'checkrole:admin', 'as' => 'admin.'], function(){
    
    Route::get('categories', ['as' => 'category.index', 'uses' => 'CategoryController@index']);
    Route::get('category/new', ['as' => 'category.new', 'uses' => 'CategoryController@create']);
    Route::post('category/store', ['as' => 'category.store', 'uses' => 'CategoryController@store']);
    Route::get('category/edit/{id}', ['as' => 'category.edit', 'uses' => 'CategoryController@edit']);
    Route::put('category/update/{id}', ['as' => 'category.update', 'uses' => 'CategoryController@update']);
    Route::get('category/destroy/{id}', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);

    Route::get('clients', ['as' => 'client.index', 'uses' => 'ClientController@index']);
    Route::get('client/new', ['as' => 'client.new', 'uses' => 'ClientController@create']);
    Route::post('client/store', ['as' => 'client.store', 'uses' => 'ClientController@store']);
    Route::get('client/edit/{id}', ['as' => 'client.edit', 'uses' => 'ClientController@edit']);
    Route::put('client/update/{id}', ['as' => 'client.update', 'uses' => 'ClientController@update']);
    Route::get('client/destroy/{id}', ['as' => 'client.destroy', 'uses' => 'ClientController@destroy']);
    
    Route::get('cupoms', ['as' => 'cupom.index', 'uses' => 'CupomController@index']);
    Route::get('cupom/new', ['as' => 'cupom.new', 'uses' => 'CupomController@create']);
    Route::post('cupom/store', ['as' => 'cupom.store', 'uses' => 'CupomController@store']);
    Route::get('cupom/edit/{id}', ['as' => 'cupom.edit', 'uses' => 'CupomController@edit']);
    Route::put('cupom/update/{id}', ['as' => 'cupom.update', 'uses' => 'CupomController@update']);
    Route::get('cupom/destroy/{id}', ['as' => 'cupom.destroy', 'uses' => 'CupomController@destroy']);
    
    Route::get('order', ['as' => 'order.index', 'uses' => 'OrderController@index']);
    Route::get('order/edit/{id}', ['as' => 'order.edit', 'uses' => 'OrderController@edit']);
    Route::put('order/update/{id}', ['as' => 'order.update', 'uses' => 'OrderController@update']);
    
    Route::get('products', ['as' => 'product.index', 'uses' => 'ProductController@index']);
    Route::get('product/new', ['as' => 'product.new', 'uses' => 'ProductController@create']);
    Route::post('product/store', ['as' => 'product.store', 'uses' => 'ProductController@store']);
    Route::get('product/edit/{id}', ['as' => 'product.edit', 'uses' => 'ProductController@edit']);
    Route::put('product/update/{id}', ['as' => 'product.update', 'uses' => 'ProductController@update']);
    Route::get('product/destroy/{id}', ['as' => 'product.destroy', 'uses' => 'ProductController@destroy']);
    
    
});

Route::group(['prefix' => 'customer', 'middleware' => 'checkrole:client', 'as' => 'customer.'], function(){
    Route::get('order', ['as' => 'order.index', 'uses' => 'CheckoutController@index']);
    Route::get('order/new', ['as' => 'order.new', 'uses' => 'CheckoutController@create']);
    Route::post('order/store', ['as' => 'order.store', 'uses' => 'CheckoutController@store']);
});

Route::group(['middleware' => 'cors'], function() {
    
    Route::post('oauth/access_token', function() {
      return Response::json(Authorizer::issueAccessToken());
    });
    
    Route::get('authenticated', function(){
        $idUser = Authorizer::getResourceOwnerId();
        return \Delivery\Models\User::find($idUser);
    });
    
    Route::group(['prefix' => 'api', 'middleware' => 'oauth', 'as' => 'api.'], function(){

        Route::group(['prefix' => 'client', 'middlewere' => 'auth.checkrole:client', 'as' => 'client.'], function(){

            Route::resource('order', 'Api\Client\ClientCheckoutController', ['except' => ['create', 'edit', 'destroy']]);
            Route::get('products', 'Api\Client\ClientProductController@index');
        });

        Route::group(['prefix' => 'deliveryman', 'middleware' => 'auth.checkrole:deliveryman', 'as' => 'deliveryman.'], function(){
            Route::resource('order', 'Api\Deliveryman\DeliverymanCheckoutController', ['except' => ['create', 'edit', 'destroy', 'store']]);

            Route::patch('order/{id}/update-status', 'Api\Deliveryman\DeliverymanCheckoutController@updateStatus', 
                    ['as' => 'orders.update_status']);
        });
    });
});


