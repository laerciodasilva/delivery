<?php

namespace Delivery\Http\Requests;

use Delivery\Http\Requests\Request;

class AdminClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|',
            'address' => 'required|min:2',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required'
            //'name' => 'required|unique:categories|min:2'
        ];
    }
}
