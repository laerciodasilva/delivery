<?php

namespace Delivery\Http\Requests\Api;

use Illuminate\Http\Request as HttpRequest;
use Delivery\Http\Requests\Request;

class CheckoutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(HttpRequest $request)
    {
        $rules = [
            'code' => 'exists:cupoms,code,used,0',
        ];
        $items = $request->get('items', []);
        $items = !is_array($items) ? [] : $items;
        if (count($items) > 1) {
            foreach ($items as $key=> $value){
                $this->buildRulesItems($key, $rules);
            }
        } else {
            $this->buildRulesItems(0, $rules);
        }
        
        return $rules;
    }
    
    public function buildRulesItems($key, array &$rules)
    {
        $rules["items.$key.product_id"] = 'required';
        $rules["items.$key.quantidade"] = 'required';
    }
}
