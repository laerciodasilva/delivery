angular.module('starter.controllers')
    .controller('ClientViewProduct', ['$scope', '$state', '$ionicLoading', 'Cart', 'Product',
        function($scope, $state, $ionicLoading, Cart, Product){
          $scope.products = [];
          $scope.totalProduto =0;
          $scope.isChecked = [];
          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });
          Product.query({}, function(data){
              $scope.products = data.data;
              $ionicLoading.hide();
          }, function(error){
              $ionicLoading.hide();
          });
          
          $scope.addProduct = function(item){
              if ($scope.isChecked[item.id]) { 
                  Cart.items.push(item);
                  $scope.totalProduto = Cart.items.length;
              } else {
                  var index = Cart.items.indexOf(item);
                  Cart.items.splice(index,1);
                  $scope.totalProduto = Cart.items.length;
              }
          };
          $scope.showProduct = function(){
            $state.go('client.checkout');  
          };
    }]);