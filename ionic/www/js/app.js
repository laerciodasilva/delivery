// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter.controllers', []);
angular.module('starter.services', []);

angular.module('starter', 
    ['ionic', 'ngResource', 'starter.controllers', 
    'starter.services', 'angular-oauth2'])
  .constant('appConfig', {
    baseUrl: 'http://delivery.local',
    baseUrlApi: 'http://delivery.local/api'
  })
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
 .config(function($stateProvider, $urlRouterProvider, OAuthProvider, OAuthTokenProvider, appConfig){
    OAuthProvider.configure({
        baseUrl: appConfig.baseUrl,
        clientId: 'app1',
        clientSecret: 'secret', // optional
        grantPath: '/oauth/access_token',
    });
    OAuthTokenProvider.configure({
        name: 'token',
        options: {
          //secure: true usar na produção com protocolo https
          secure: false
        }
    });
     $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'Login'
     })
     .state('home', {
        url: '/home',
        templateUrl:'templates/home.html'
     })
     .state('client', {
        abstract: true,
        url: '/client',
        template: '<ui-view/>'
     })
        .state('client.checkout', {
           url: '/checkout',
           templateUrl: 'templates/client/checkout.html',
           controller: 'ClientCheckout'
        })
        .state('client.checkout_detail', {
           url: '/checkout/detail/:id',
           templateUrl: 'templates/client/checkout_detail.html',
           controller: 'ClientCheckoutDetail'
        })
        .state('client.view_product', {
           url: '/view_products',
           templateUrl: 'templates/client/view_products.html',
           controller: 'ClientViewProduct'
        });
     $urlRouterProvider.otherwise('/home');
 })
 .service('Cart', function(){
     this.items = [];
 });
