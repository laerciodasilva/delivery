angular.module('starter.services')
  .factory('Product', ['$resource', 'appConfig',
    function($resource, appConfig){
      return $resource(appConfig.baseUrlApi+'/client/products', {}, {
        query: {
          isArray: false
        }
      });
                        
  }]);